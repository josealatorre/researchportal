# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('plotly', '0006_remove_plotly_file'),
    ]

    operations = [
        migrations.AddField(
            model_name='plotly',
            name='file',
            field=models.FileField(default=1, upload_to='uploads/'),
            preserve_default=False,
        ),
    ]
