# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0018_pagenode'),
        ('plotly', '0004_auto_20180312_1833'),
    ]

    operations = [
        migrations.CreateModel(
            name='Plotly',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(related_name='plotly_plotly', auto_created=True, primary_key=True, serialize=False, parent_link=True, to='cms.CMSPlugin')),
                ('file', models.FileField(upload_to='uploads/')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.RemoveField(
            model_name='social',
            name='cmsplugin_ptr',
        ),
        migrations.DeleteModel(
            name='Social',
        ),
    ]
