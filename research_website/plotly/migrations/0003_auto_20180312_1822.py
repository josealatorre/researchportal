# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0018_pagenode'),
        ('plotly', '0002_auto_20180312_1811'),
    ]

    operations = [
        migrations.CreateModel(
            name='Icon',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Social',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(serialize=False, auto_created=True, primary_key=True, to='cms.CMSPlugin', parent_link=True, related_name='plotly_social')),
                ('label', models.CharField(max_length=200, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='SocialIcon',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(serialize=False, auto_created=True, primary_key=True, to='cms.CMSPlugin', parent_link=True, related_name='plotly_socialicon')),
                ('url_link', models.URLField()),
                ('link_title', models.CharField(max_length=200, blank=True)),
                ('extra_classes', models.CharField(max_length=200, blank=True)),
                ('icon', models.ForeignKey(to='plotly.Icon')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.RemoveField(
            model_name='plotly',
            name='cmsplugin_ptr',
        ),
        migrations.DeleteModel(
            name='Plotly',
        ),
    ]
