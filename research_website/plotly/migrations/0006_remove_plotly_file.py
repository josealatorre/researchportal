# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('plotly', '0005_auto_20180312_1835'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='plotly',
            name='file',
        ),
    ]
