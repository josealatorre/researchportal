# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0018_pagenode'),
        ('plotly', '0003_auto_20180312_1822'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='socialicon',
            name='cmsplugin_ptr',
        ),
        migrations.RemoveField(
            model_name='socialicon',
            name='icon',
        ),
        migrations.RemoveField(
            model_name='social',
            name='label',
        ),
        migrations.AddField(
            model_name='social',
            name='file',
            field=models.FileField(upload_to='uploads/', default=1),
            preserve_default=False,
        ),
        migrations.DeleteModel(
            name='Icon',
        ),
        migrations.DeleteModel(
            name='SocialIcon',
        ),
    ]
