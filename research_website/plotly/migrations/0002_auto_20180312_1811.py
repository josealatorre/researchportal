# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0018_pagenode'),
        ('plotly', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Plotly',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(related_name='plotly_plotly', auto_created=True, parent_link=True, primary_key=True, to='cms.CMSPlugin', serialize=False)),
                ('plotly_file', models.CharField(help_text='Please supply something', default='', max_length=300, verbose_name='Plotly File')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.RemoveField(
            model_name='social',
            name='cmsplugin_ptr',
        ),
        migrations.RemoveField(
            model_name='socialicon',
            name='cmsplugin_ptr',
        ),
        migrations.RemoveField(
            model_name='socialicon',
            name='icon',
        ),
        migrations.DeleteModel(
            name='Icon',
        ),
        migrations.DeleteModel(
            name='Social',
        ),
        migrations.DeleteModel(
            name='SocialIcon',
        ),
    ]
