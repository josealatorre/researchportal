# -*- coding: utf-8 -*-
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from . import models


class PlotlyPlugin(CMSPluginBase):
    model = models.Plotly
    name = 'Plotly Chart'
    render_template = 'plotly/plotly_plugin.html'



plugin_pool.register_plugin(PlotlyPlugin)
