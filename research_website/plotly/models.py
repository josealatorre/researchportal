# -*- coding: utf-8 -*-
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.core.files.storage import default_storage
from cms.models import CMSPlugin


@python_2_unicode_compatible
class Plotly(CMSPlugin):
    file = models.FileField(
        upload_to='uploads/'
    )

    def __str__(self):
        uploaded_file = default_storage.open(self.file.name)
        file_text = uploaded_file.read()
        return str(file_text)
